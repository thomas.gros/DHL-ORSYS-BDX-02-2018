let a = document.querySelector('a');
console.log(a.hasAttribute('href'));
console.log(a.getAttribute('href'));

a.setAttribute('href', 'http://www.example.com');

console.log(a.firstChild);
console.log(a.firstChild.nodeValue);

let p = document.querySelector('p');
console.log(p.firstChild);
console.log(p.firstChild.nodeName);
console.log(p.firstChild.nodeValue);

console.log(p.childNodes);

for(n of p.childNodes) {
    console.log(n);
}

console.log(p.parentNode);

p.innerHTML = "hello <span>world</span>";

for(let i = p.childNodes.length - 1; i >= 0; i--) {
    let c = p.childNodes.item(i);
    p.removeChild(c);
}

let n1 = document.createTextNode("hola ");
let n2 = document.createElement("span");
let n3 = document.createTextNode("mundo");
n2.appendChild(n3);
p.appendChild(n1);
p.appendChild(n2);

console.log(p.innerHTML);
console.log(p.outerHTML);
console.log(p.textContent);
console.log(p.innerText);