let thomas = { // syntaxe literalle
    "firstname": "thomas",
    "lastname": "gros",
    "test": function () {
        console.log('test')
    },
    "fullname": function () {
        return `${this.firstname} ${this.lastname}`; // template literals ES6
    }
};

console.log(thomas);
console.log(thomas.firstname);
console.log(thomas["firstname"]);
thomas.test();
console.log(thomas.fullname());

let f = thomas.fullname;
console.log(f());

let g = thomas.fullname.bind(thomas); // bind copie la fonction en 'liant' this à une valeur spécifiée, ici thomas.
console.log(g());