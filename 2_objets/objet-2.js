// let thomas = {
//     "firstname": "thomas",
//     "lastname": "gros",
//     "fullname": function () {
//         return `${this.firstname} ${this.lastname}`;
//     }
// };
//
// let richard = {
//     "firstname": "richard",
//     "lastname": "moglia",
//     "fullname": function () {
//         return `${this.firstname} ${this.lastname}`;
//     }
// };

function Person(firstname, lastname) { // fonction constructeur commencent par une majuscule
    this.firstname = firstname;
    this.lastname = lastname;
}

Person.prototype.fullname = function() {
    return `${this.firstname} ${this.lastname}`;
};

// 1) crée un objet vide {}
// 2) Appelle la fonction constructeur (ici Person)
//    avec this = l'objet construit en 1)
// 3) référence Person.prototype comme prototype (__proto__)
// de l'objet créé en 1)
// {}.__proto__ = Person.prototype;
let thomas = new Person('thomas', 'gros');
let richard = new Person('richard', 'moglia');


console.log(thomas);
console.log(richard);
console.log(thomas.firstname);
console.log(thomas.fullname());
console.log(thomas.test);


