class Person { // classe ES6, sucre syntaxique sur les fonctions constructeurs
    constructor(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    fullname() {
        return `${this.firstname} ${this.lastname}`;
    };
}

let thomas = new Person('thomas', 'gros');
let richard = new Person('richard', 'moglia');

console.log(thomas);
console.log(richard);
console.log(thomas.firstname);
console.log(thomas.fullname());