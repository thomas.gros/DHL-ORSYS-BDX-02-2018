/*** show-hide */
let shB = document.querySelector("div.showhide button");
let shP = document.querySelector("div.showhide p");

shB.addEventListener('click', function(e) {
    shP.hidden = ! shP.hidden;
    shB.innerHTML = shP.hidden ? "show" : "hide";
});

/*** change style */
let csA = document.querySelector("div.changestyle a");
let csD = document.querySelector("div.changestyle div");

csA.addEventListener('click', function(e) {
    e.preventDefault();

    console.log(csD.style.backgroundColor);
    // csD.style.backgroundColor = 'blue'
    // csD.setAttribute("class", "changestyle-blue");
    csD.classList.toggle('changestyle-blue')
});

/*** form */
let form = document.querySelector('#login-form');
form.addEventListener('submit', function(e) {
    e.preventDefault();
    // verifications...

    // si ca passe alors appeler form.submit()
    // form.submit();
});

let usernameInput = document.querySelector('#login-form-username');

usernameInput.addEventListener('keyup', function(e) {
    let usernameError = document.querySelector('#login-form-username-error');

    if(validateUsername(usernameInput.value)) {
        usernameError.innerHTML = '';
    } else {
        usernameError.innerHTML = "erreur saisie username"
    }
});

function validateUsername(username) {
    let pattern = /^[a-z]+$/;
    return pattern.test(username);
}