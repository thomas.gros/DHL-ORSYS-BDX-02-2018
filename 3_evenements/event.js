let p = document.querySelector('p');

let clickListener = function(e) {
    console.log('clicked sur my paragraph');
    console.log(e);
};

let mousemoveListener = function(e) {
    console.log('moving...');
    console.log(e);
};

p.addEventListener('click', clickListener);
// p.addEventListener('mousemove', mousemoveListener);
// p.removeEventListener('mousemove', mousemoveListener)

let a = document.querySelector('a');
a.addEventListener('click', function(e) {
    e.preventDefault();
    console.log('clické sur le lien');
});


let outerDiv = document.querySelector("div.outer-div");
let innerDiv = document.querySelector("div.inner-div");


document.body.addEventListener('click', function(e) {
    console.log("clicked on BODY", e);
    console.log(e.target);
    console.log(e.currentTarget);
    console.log(e.srcElement);
});

outerDiv.addEventListener('click', function(e) {
    console.log("clicked on OUTER div", e);
    console.log(e.target);
    console.log(e.currentTarget);
    console.log(e.srcElement);
});

innerDiv.addEventListener('click', function(e) {
    console.log("clicked on INNER div", e);
    console.log(e.target);
    console.log(e.currentTarget);
    console.log(e.srcElement);
});











