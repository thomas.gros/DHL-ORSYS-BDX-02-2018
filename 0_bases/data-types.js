// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures

let my_var; // undefined

my_var = null; // null
my_var = 1; // nombres
my_var = 'hello'; // string
my_var = true; // booleans
my_var = [1,2,3]; // array

my_var = { // objets
    "firstname": "thomas",
    "lastname": "gros",
    "happy": true,
    "emails": ["t.g@gmail.com", "t2.g@example.com"]
};

console.log(my_var);
my_var.lastname;
