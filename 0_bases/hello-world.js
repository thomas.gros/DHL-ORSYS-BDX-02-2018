'use strict'; // string mode
console.log('hello');
console.log('world');

let x = 42; // variables ES6. Utiliser var si plus ancien JavaScript
console.log(x);
console.log('end...');

const TRAINING_NAME = 'dhl'; // constantes ES6
console.log(TRAINING_NAME);
