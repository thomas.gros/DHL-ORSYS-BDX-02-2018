function updateP() {
    let p = document.querySelector('p'); // sélectioner le premier p dans le DOM
    p.innerHTML = 'test ?'; // mettre à jour le contenu HTML du paragraphe
}

let title = document.querySelector('h1'); // sélectioner le premier h1 dans le DOM
title.addEventListener('click', updateP); // ajouter un listener sur le click sur le h1.