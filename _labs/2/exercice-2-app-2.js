function personToHTML(p) {
    return `<tr>
                <td>${p.firstname}</td>
                <td>${p.lastname}</td>
             </tr>`;
}

// .then voir les Promises ES6
fetch('http://localhost:3000/persons')
        .then(response => response.json())
        .then(json => {
        let html = `<table class="person">`;
        html += json.map(personToHTML).join('');
        html += `</table>`;

        let personListDiv = document.querySelector('div.list');
        personListDiv.innerHTML = html;

        updateDetail(0);
    });

function updateDetail(id) {
    fetch(`http://localhost:3000/persons/${id}`)
        .then(response => response.json())
        .then(json => {
            document.querySelector('div.detail').innerHTML = JSON.stringify(json)
        });
}