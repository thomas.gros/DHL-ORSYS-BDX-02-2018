let thomas = new Person('thomas', 'gros');
let richard = new Person('richard', 'moglia');

let persons = [thomas, richard];

function personToHTML(p) {
    return `<tr>
                <td>${p.firstname}</td>
                <td>${p.lastname}</td>
             </tr>`;
}

let html = `<table class="person">`;
html += persons.map(personToHTML).join('');
html += `</table>`;

let personListDiv = document.querySelector('div.list');
personListDiv.innerHTML = html;