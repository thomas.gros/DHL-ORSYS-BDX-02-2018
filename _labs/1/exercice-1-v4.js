class News {
    constructor(title, category, time) {
        this.title = title;
        this.category = category;
        this.time = time;
    }
}

let news1 = new News("Pouille elimine Gulbis","Tennis - ATP - Dubaï", "15:38");
let news2 = new News("Neymar: Emery dément l'opération", "Foot - L1 - PSG", "14:08");

let news = [news1, news2];

function newsToHTML(n) {
    return `<tr>
                <td>${n.time}</td>
                <td class="news__cat">${n.category}</td>
                <td>${n.title}</td>
             </tr>`;
}

let html = `<table class="news">`;
html += news.map(newsToHTML).join('');
html += `</table>`;

document.body.innerHTML = html;