let news1 = {
    "title": "Pouille elimine Gulbis",
    "category": "Tennis - ATP - Dubaï",
    "time": "15:38"
};

let news2 = {
    "title": "Neymar: Emery dément l'opération",
    "category": "Foot - L1 - PSG",
    "time": "14:08"
};

let news = [news1, news2];

function newsToHTML(n) {
    return `<tr>
                <td>${n.time}</td>
                <td class="news__cat">${n.category}</td>
                <td>${n.title}</td>
             </tr>`;
}

let html = `<table class="news">`;
html += news.map(newsToHTML).join('');
html += `</table>`;

document.body.innerHTML = html;