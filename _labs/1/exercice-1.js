/*
    1) Cree une object,
    qui permet de représenter une Personne.
    Personne: nom, prenom, age, adresse.
*/


/*let personne = {
    "firstname": "Steve",
    "lastname": "Jobs",
    "age": 56,
    "adresse": "1 rue de la pomme"
};

document.body.innerHTML =
    "<div>" +
        "<p>" + personne.firstname + "</p>" +
        "<p>" + personne.lastname + "</p>" +
        "<p>" + personne.age + "</p>" +
    "</div>";

document.body.innerHTML =
    `<div>
        <p>${personne.firstname}</p>
        <p>${personne.lastname}</p>
        <p>${personne.age}</p>
    </div>`;*/

let news1 = {
    "title": "Pouille elimine Gulbis",
    "category": "Tennis - ATP - Dubaï",
    "time": "15:38"
};

let news2 = {
    "title": "Neymar: Emery dément l'opération",
    "category": "Foot - L1 - PSG",
    "time": "14:08"
};

let news = [news1, news2];
console.log(news);


let html = `<table class="news">`;

/*
    for(let i = 0; i < news.length; i++) {
        let n = news[i];

        html += `<tr>
                    <td>${n.time}</td>
                    <td class="news__cat">${n.category}</td>
                    <td>${n.title}</td>
                 </tr>`
}*/

for(let n of news) {
    html += `<tr>
                <td>${n.time}</td>
                <td class="news__cat">${n.category}</td>
                <td>${n.title}</td>
             </tr>`
}

html += `</table>`;

document.body.innerHTML = html;

/*document.body.innerHTML =
    `<table class="news">
        <tr>
            <td>${news1.time}</td>
            <td class="news__cat">${news1.category}</td>
            <td>${news1.title}</td>
        </tr>
        <tr>
            <td>${news2.time}</td>
            <td class="news__cat">${news2.category}</td>
            <td>${news2.title}</td>
        </tr>
    </table>`;*/