// declaration d'une fonction
function hello() {
    return 'hello'
}

// invocation / execution d'une fonction
let res = hello();
console.log(res);

console.log(hello());

// une fonction peut etre un parametre d'une fonction
console.log(hello);

let a = 42;
let b = hello; // une fonction est un citoyen de premier ordre

console.log(b);
console.log(b());

// parametres de fonction
function hello2(name) {
    return 'hello ' + name;
}

console.log(hello2('toto'));

function add(a, b) {
    console.log(a);
    console.log(b);
    console.log(arguments);
    return a + b;
}

console.log(add(1,2));
console.log(add(1,2,3,4,5,6));
console.log(add(1));
console.log(add());

// arguments
function add2() {
    console.log(arguments);
    let total = 0;
    for(let i of arguments) {
        total += i;
    }
    return total;
}

console.log(add2(1,2,3,4,5));
