/*** for each ***/
function foreach(t, f) {
    for(let i of t) {
        f(i);
    }
}

foreach([1,2,3], console.log);

/*** map ***/
function map(t, f) {
    let result = [];
    for(let e of t) {
        result.push(f(e));
    }
    return result;
}

function square(i) {
    return i * i;
}
console.log(map([1,2,3], square));

let personnes = [
    {
        "firstname": "thomas",
        "lastname": "gros",
        "age" : 38
    },
    {
        "firstname": "richard",
        "lastname": "moglia",
        "age": 31
    }
];

/*
Exercice:
à partir du tableau de personnes
Retourner un tableau des fullname des personnes
cad ["thomas gros", "richard moglia"]
 */
function toFullName(p) {
    return `${p.firstname} ${p.lastname}`;
}

console.log(map(personnes, toFullName));

function toUpper(s) {
    return s.toUpperCase()
}

console.log(map(map(personnes, toFullName), toUpper));

/*** filter ***/
function filter(t, f) {
    let result = [];

    for(let e of t) {
        if(f(e)) {
            result.push(e)
        }
    }

    return result;
}

function isYoung(p) {
    return p.age < 35;
}

console.log(filter(personnes, isYoung));

console.log(map(filter(personnes, isYoung), toFullName));

/*** filter, map, etc... en JavaScript ***/

let aucarre = [1,2,3].map(square);
console.log(aucarre);

let youngPersonsFullNames = personnes
                                .filter(isYoung)
                                .map(toFullName);
