// function square(i) {
//     return i * i;
// }
//
// let f = square;

// fonctions anonymes dans les expressions de fonction
let square = function (i) {
    return i * i
};

square(4);

[1,2,3].map(function(i) { return i * i});

// ES6 Arrow function (lambda functions dans d'autres langages)
[1,2,3]
    .map(e => e * e)
    .filter(e => e > 5);



x = 3;
result = square(x);
assertEquals(9, result);

x = 0
result = square(0)
assertEquals(0, result);








