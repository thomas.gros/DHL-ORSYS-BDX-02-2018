let http = require('http');

let Person = require('./person');

let thomas = new Person('thomas', 'gros');
let richard = new Person('richard', 'moglia');
let persons = [thomas, richard];

function handler(request, response) {
    if("/persons" == request.url) {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Content-Type', 'application/json');
        response.end(JSON.stringify(persons));
    } else if ("/persons/0" == request.url) {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Content-Type', 'application/json');
        response.end(JSON.stringify(persons[0]));
    } else {
        let headers = {
            'Content-Type': 'text/html',
            'Access-Control-Allow-Origin': '*'
        };

        response.writeHead(404, headers);
        response.end('page not found');
    }
}

let server = http.createServer(handler);
server.listen(3000);















