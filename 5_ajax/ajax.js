let button = document.querySelector('.fetch-ip');
button.addEventListener('click', function (e) {
    fetchAndDisplayIp();
});

function fetchAndDisplayIp() {
    // 1) obtenir un objet qui va nous permettre
    // de faire une requete HTTP - XMLHttpRequest

    let xhr = new XMLHttpRequest();

    // 2) accrocher un listener pour reagir
    // quand la reponse HTTP revient

    /// xhr.onreadystatechange = //...

    xhr.addEventListener('readystatechange', function (e) {
        if (xhr.readyState == 4) {
            // travailler avec la reponse HTTP
            let json = xhr.responseText;
            print(json);
            let ip = JSON.parse(json);
            document.querySelector('.result').innerHTML = ip.origin
        }
    });

    // 3) emettre la requete HTTP
    xhr.open('GET', 'https://httpbin.org/ip', true);
    xhr.send(null);
}